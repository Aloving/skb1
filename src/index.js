import express from 'express';

var app = express();

app.get('/', function(req, res){

  function parseUrl(url){
    let a,b;

    function plusFunc (a, b){
      if(Number(a) && a != NaN){
        a = a;
      }else{
        a = 0;
      }
      if(Number(b) && b !== NaN){
        b = b;
      }else{
        b = 0;
      }
      return +a + +b;
    }

    if(typeof url == "object"){
      a = url.a;
      b = url.b;
      return plusFunc(a, b);
    }else{
      return false;
    }


  };

  res.send(parseUrl(req.query) + "");


});

app.listen(3000, function(){
  console.log('Server is listening at 3000 port');
});